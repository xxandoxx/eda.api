#!/bin/bash

#cd /home/admin/web/api.okaygorod.com

#path=`pwd -P`

parentDir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )"/../ ;

if [ `whoami` == "root" ]; then
       sudo su ubuntu -c "$parentDir ;
        git fetch origin ;
        git reset --hard origin/master ;
        php artisan migrate ;
        "
else
        cd $parentDir
        git fetch origin
        git reset --hard origin/master
        php artisan migrate
#        php74 ./composer.phar dump-autoload -o
#        php artisan l5-swagger:generate
fi

echo 'finish';

