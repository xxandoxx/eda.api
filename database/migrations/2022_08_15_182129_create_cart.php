<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('cart', function (Blueprint $table) {
//            $table->string('token', 50);
//            $table->string('name', 500)->nullable(false);
//            $table->longText('cart')->nullable(false);
//            $table->float('price')->nullable(false);
//            $table->integer('line_item_count')->nullable(false);
//            $table->bigInteger('auto_increment', true, true)->unique();
//            $table->timestamps();
//
//            /** @help https://stackoverflow.com/questions/42929776/how-to-set-auto-increment-into-non-primary-key */
//            $table->dropPrimary("auto_increment");
//            $table->primary(['token']);
//        });

        $sql = 'CREATE TABLE `cart` (
  `token` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `line_item_count` int(11) NOT NULL,
  `auto_increment` int(11) NOT NULL AUTO_INCREMENT UNIQUE,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;';
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
