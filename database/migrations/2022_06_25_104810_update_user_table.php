<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {            
            $table->string('surname', 80)->after('name')->nullable()->default(null);
            $table->string('patronymic', 80)->after('surname')->nullable()->default(null);
            $table->date('birthday', 80)->after('patronymic')->nullable()->default(null);
            $table->string('phone', 80)->after('birthday')->unique()->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
