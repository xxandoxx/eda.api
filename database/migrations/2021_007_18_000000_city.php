<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class City extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('name_en')->unique();
            $table->boolean('active')->nullable();
            $table->float('x', 13, 8);
            $table->float('y', 13, 8);
            $table->char('alpha_2', 4);
            $table->string('country_name');
            $table->mediumInteger('country_code', false, true);
            $table->string('federal_subject');
            $table->string('time_zone');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
