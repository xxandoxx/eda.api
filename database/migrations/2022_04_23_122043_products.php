<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_id');
            $table->string('name');
            $table->integer('cost');
            $table->string('cost_label')->default('₽');
            $table->text('description')->nullable();
            $table->text('composition_of_goods')->nullable();
            $table->boolean('active')->default(1);
            $table->string('vendor_code')->nullable();
            $table->integer('weight')->nullable();
            $table->string('weight_label')->nullable();
            $table->integer('remainder')->nullable();
            $table->bigInteger('creator_id')->unsigned();
            $table->string('another_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
