<?php
// Auth::loginUsingId(1);
// use App\Http\Controllers\Client\City\CityClientController;


use App\Http\Controllers\Client\Cart\CartController;
use App\Http\Controllers\Client\City\CityClientController;
use App\Http\Controllers\Management\City\CityController;
use App\Http\Controllers\Management\Shop\ShopController;
use App\Http\Controllers\Management\Media\MediaController;
use App\Http\Controllers\Management\Category\CategoryController;
use App\Http\Controllers\Management\Product\ProductController;
use App\Http\Controllers\Management\User\UserController;
use App\Http\Controllers\Management\Information\InformationController;
use App\Http\Middleware\Management\CityAccess;
use App\Http\Middleware\SuperAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::middleware('auth:sanctum')->resource('/city', CityController::class);
Route::prefix('management')->middleware(['auth:sanctum'])->group(function () {
    //Super Admin Accesses
    Route::middleware([SuperAdmin::class])->group(function () {
        Route::resource('/city', CityController::class);
        Route::resource('/user', UserController::class);
        // Route::post('/user/create', [UserController::class, 'createUser']);
        Route::resource('/information', InformationController::class);
    });

    Route::resource('/media', MediaController::class);

    Route::prefix('{city_name}')->group(function () {

        Route::middleware([CityAccess::class])->group(function () {
            Route::resource('/shop', ShopController::class);
            Route::resource('/{shop_id}/product', ProductController::class);
            Route::resource('/{shop_id}/category', CategoryController::class);
        });

    });

});
//Route::middleware(['auth:sanctum'])->group(function () {
//
//
//});

/**
 * Client
 */
Route::prefix('user')->group(function () {
    Route::post('/registration', [\App\Http\Controllers\Client\User\UserController::class, 'registration']);
    Route::post('/login', [\App\Http\Controllers\Client\User\UserController::class, 'login']);
    Route::get('/verifyEmail/{id}/{hash}', [\App\Http\Controllers\Client\User\UserController::class, 'verifyEmail'])
        ->middleware(['auth', 'signed', 'throttle:6,1']);
});

Route::resource('/city', CityClientController::class);
Route::resource('/information', \App\Http\Controllers\Client\Information\InformationController::class);

Route::prefix('cart')->group(function () {
    Route::post('/add', [CartController::class, 'add']);
});
