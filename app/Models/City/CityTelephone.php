<?php


namespace App\Models\City;


use App\Models\AppModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CityTelephone extends AppModel
{
    public $timestamps = false;

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }
}
