<?php

namespace App\Models\City;

use App\Models\AppModel;
use App\Models\Shop\Shop;
use Illuminate\Database\Eloquent\Relations\HasMany;



class City extends AppModel
{

    // protected $primaryKey = 'name_en';
    // protected $keyType = 'string';
    protected $with = ['telephones'];
    protected array $mediaFields = ['flag'];

    public function telephones(): HasMany
    {
        return $this->hasMany(CityTelephone::class);
    }

    public function shops(): HasMany
    {
        return $this->hasMany(Shop::class);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name_en';
    }
}
