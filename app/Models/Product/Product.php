<?php

namespace App\Models\Product;

use App\Models\AppModel;
use App\Models\Category\Category;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends AppModel
{
    protected $with = ['prices','categories'];
    protected array $mediaFields = ['images'];
    protected $casts = [
        'active' => 'boolean',
        'order_min' => 'float',
        'can_card' => 'boolean',
        'blocked' => 'boolean',
    ];

    protected $appends = ['images'];

    public function prices(): HasMany
    {
        return $this->hasMany(ProductPrice::class);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }
}
