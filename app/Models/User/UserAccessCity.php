<?php

namespace App\Models\User;

use App\Models\AppModel;
use App\Models\City\City;
use App\Models\User;

class UserAccessCity extends AppModel
{
    public $timestamps = false;

    protected $fillable = [
        'city_id',
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function cities(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(City::class);
    }
}
