<?php

namespace App\Models\Shop;

use App\Models\AppModel;
use App\Models\Product\Product;
use App\Models\Category\Category;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Shop extends AppModel
{
    protected $with = [];
    protected array $mediaFields = ['logo'];
    protected $casts = [
        'active' => 'boolean',
        'order_min' => 'float',
        'can_card' => 'boolean',
        'blocked' => 'boolean',
    ];

    protected $appends = ['logo'];

    public function categories(): HasMany
    {
        return $this->hasMany(Category::class);
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

}
