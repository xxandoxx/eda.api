<?php

namespace App\Models;

use App\Models\AppModel;
use App\Models\City\City;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use App\Media\MediaInterface;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * @property bool $isSuperAdmin
 */
class User extends AppModel implements 
    HasMedia,
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail;
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use InteractsWithMedia;
    protected array $mediaFields = ['temp'];  

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'patronymic',
        'birthday',
        'phone',
        'email',
        'password',
        'email_verified_at',
        'isSuperAdmin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'birthday' => 'date',
        'email_verified_at' => 'datetime',
        'isSuperAdmin' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

//    protected $with = ['accessCities'];

    public function accessCities(): BelongsToMany
    {
        return $this->belongsToMany(City::class, 'user_access_cities');
    }

    /**
     * @param string $city_name
     * @return bool
     */
    public function hasAccessToCity(string $city_name): bool
    {
        if ($this->isSuperAdmin) {
            return true;
        }
        return (bool)$this->accessCities()->where('name_en', $city_name)->count('id');
    }

    public function hasAccessToShop(int $shopId): bool
    {
        //TODO in future
    }

    public function registerMediaConversions(Media $media = null): void
    {
        foreach (MediaInterface::THUMBNAILS as $key => $value) {
            $this->addMediaConversion($key)
                ->width($value['width'])
                ->height($value['height'])
                ->quality($value['quality'] ?? 98)
                ->sharpen($value['sharpen'] ?? 10);
        }
    }

    public function getImagesAttribute(): array
    {
        $images = [];
        $mediaItems = $this->getMedia('temp');
        foreach($mediaItems as $key => $image) {
            $images[$key]['small'] = $image->getUrl('small') ?? null;
            $images[$key]['medium'] = $image->getUrl('medium') ?? null;
            $images[$key]['large'] = $image->getUrl('large') ?? null;
            $images[$key]['original'] = $image->original_url;
        }
        return $images;

    }
}
