<?php

namespace App\Models\Information;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AppModel;

class Information extends AppModel
{
    use HasFactory;

    protected $table = 'information';
    protected $appends = [];
}
