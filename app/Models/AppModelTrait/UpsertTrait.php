<?php

namespace App\Models\AppModelTrait;

use App\Media\MediaCreator;
use App\Models\AppModel;
use Illuminate\Database\Eloquent\Relations\HasOneOrMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use ReflectionMethod;

trait UpsertTrait
{

    /**
     * @param array $item
     * @return AppModel|null
     * @throws \Exception
     */
    public function upsert(array $item)
    {
        if (empty($item)) throw new \Exception('data is empty');
        $relation = [];
        $images = [];
        
        $model = ($this->getAttribute('id')) ? $this : self::create();
        foreach ($item as $key => $value) {
            if (!empty($this->mediaFields) && in_array($key, $this->mediaFields)) {
                $images[$key] = $value;
            } elseif (is_array($value)) {
                $relation[$key] = $value;
            } else {
                $model->$key = $value;
            }
        }
        $model->save();
        foreach ($relation as $key => $value) {
            self::relationUpsert($model, $key, $value);
        }
        
        $media = new MediaCreator($model);
        $media->addMediaUpsert($images);

        return $model->refresh();
    }

    /**
     * @param AppModel $model
     * @param string $method
     * @param array $value
     * @throws \Exception
     */
    private static function relationUpsert(self $model, string $method, array $value)
    {
        if (!method_exists($model, $method)) {
            throw new \Exception('Can not found relation "' . $method . '"');
        }
        $relation = $model->$method();
        if ($relation instanceof HasOneOrMany) {

            self::relationHasOneOrMany($relation, $value);

        } else if($relation instanceof BelongsToMany) {

            self::relationBelongsToMany($relation, $value);
            
        }


        $reflectionMethod = new ReflectionMethod($model, $method);
        $type = $reflectionMethod->getReturnType();
    }

    private static function relationHasOneOrMany(HasOneOrMany $relation, array $value)
    {
        $relation->delete();
        $relation->createMany($value);
    }

    private static function relationBelongsToMany(BelongsToMany $relation, array $value)
    {
        $relation->sync($value);        
    }
}
