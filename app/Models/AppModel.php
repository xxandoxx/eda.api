<?php


namespace App\Models;


use App\Media\MediaInterface;
use App\Models\AppModelTrait\UpsertTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @method static create()
 * @method static paginate(int $int)
 * @method findOrFail(string|int $id)
 */
class AppModel extends Model implements HasMedia
{
    use UpsertTrait, InteractsWithMedia;

    protected $with = [];
    // protected array $mediaFields = ['image'];
    protected $hidden = ['media'];

    protected $guarded = [];

    /**
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        foreach (MediaInterface::THUMBNAILS as $key => $value) {
            $this->addMediaConversion($key)
                // ->width($value['width'])
                // ->height($value['height'])
                ->fit(Manipulations::FIT_FILL, $value['width'], $value['height'])
                ->quality($value['quality'] ?? 98)
                ->sharpen($value['sharpen'] ?? 10);
        }
    }

    public function getLogoAttribute()
    {
        $image = null;

        $mediaItems = $this->getMedia('logo')->first();

        if($mediaItems) {
            $image['id'] = $mediaItems->id;
            $image['small'] = $mediaItems->getUrl('small') ?? null;
            $image['medium'] = $mediaItems->getUrl('medium') ?? null;
            $image['large'] = $mediaItems->getUrl('large') ?? null;
            $image['original'] = $mediaItems->original_url;
        
        }

        return $image;
    }

    public function getImagesAttribute(): array
    {
        $images = [];
        $mediaItems = $this->getMedia('images');
        foreach($mediaItems as $key => $image) {
            $images[$key]['id'] = $image->id;
            $images[$key]['small'] = $image->getUrl('small') ?? null;
            $images[$key]['medium'] = $image->getUrl('medium') ?? null;
            $images[$key]['large'] = $image->getUrl('large') ?? null;
            $images[$key]['original'] = $image->original_url;
        }
        return $images;

    }
}
