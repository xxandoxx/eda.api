<?php

namespace App\Models\Category;

use App\Models\AppModel;
use App\Models\Product\Product;
use App\Models\Shop\Shop;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends AppModel
{
    protected $with = [];
    protected array $mediaFields = [];
    protected $casts = [
        'active' => 'boolean'
    ];

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }
}
