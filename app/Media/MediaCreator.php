<?php

namespace App\Media;

use App\Models\AppModel;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Exceptions\InvalidBase64Data;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaCreator implements MediaInterface
{
    /**
     * @var \App\Models\AppModel
     */
    private $model;

    /**
     * @param \App\Models\AppModel $model
     */
    public function __construct(AppModel $model)
    {
        $this->model = $model;
    }

    public function addMediaUpsert(array $data)
    {
        foreach ($data as $collectionName => $image) {

            if(is_array($image)) {
                $mediaCollection = $this->model->getMedia($collectionName)->pluck('id')->toArray();

                // if(count($image)) {
                    $images = $this->model->getMedia($collectionName)->whereNotIn('id', $image);
                    foreach ($images as $media) {
                        $media->forceDelete();
                    }
                // }

                foreach($image as $key => $item) {

                    if ($item === null) continue;

                    if(in_array($item, $mediaCollection)) continue;

                    try {
                        $this->setId($collectionName, $item);  
                    } catch (\Exception $e) {
                        continue;
                    }

                }

            } else {
                
                if ($image === null) continue;
                $media = $this->model->getMedia($collectionName)->where('id', $image)->first();
                
                if($media) {
                    continue;
                } else {
                    $media = $this->model->clearMediaCollection($collectionName);

                    try {
                        $this->setId($collectionName, $image);  
                    } catch (\Exception $e) {
                        continue;
                    }
                }

            }
           

        }

    }

    // public function addMediaUpsert(array $data)
    // {
    //     foreach ($data as $collectionName => $image) {
    //         $mediaCollection = $this->model->getMedia($collectionName);
    //         /** @var Media $media */
    //         foreach ($mediaCollection as $media) {
    //             $media->forceDelete();
    //         }
    //         if ($image === null) continue;
    //         foreach (self::TYPE as $type) {

    //             if (!empty($image) && !empty($image[$type])) {
    //                 $dataArray = (is_array($image[$type])) ? $image[$type] : [$image[$type]];
    //                 foreach ($dataArray as $data) {
    //                     $methodName = 'set' . $type;
    //                     $this->$methodName($collectionName, $data);
    //                 }
    //                 break;
    //             }
    //         }

    //     }

    // }

    /**
     * @param string $collectionName
     * @param string $url
     * @throws FileCannotBeAdded
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function setUrl(string $collectionName, string $url): void
    {
        $this->model->addMediaFromUrl($url)->toMediaCollection($collectionName);
    }


    /**
     * @param string $collectionName
     * @param string $dataBase64
     * @throws FileCannotBeAdded
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     * @throws InvalidBase64Data
     */
    public function setBase64(string $collectionName, string $dataBase64): void
    {
        $this->model->addMediaFromBase64($dataBase64)->toMediaCollection($collectionName);
    }

    public function setRequest(string $collectionName, object $file): void
    {
        $this->model->addMediaFromRequest($file)->toMediaCollection($collectionName);
    }

    public function setId(string $collectionName, int $id): void
    {
        /** @var Media $mediaItem */
        $mediaItem = request()->user()->media()->findOrFail($id);
        $mediaItem->move($this->model, $collectionName);
    }
}
