<?php

namespace App\Media;

interface MediaInterface
{
    /**
     * priority by index
     */
    const TYPE = [
        0 => 'url',
        1 => 'base64',
        2 => 'id'
    ];

    const THUMBNAILS = [
        'small' => [
            'width' => 200,
            'height' => 200,
            'quality' => 90,
            'sharpen' => 0,
        ],
        'medium' => [
            'width' => 500,
            'height' => 500,
            'quality' => 90,
            'sharpen' => 0,
        ],
        'large' => [
            'width' => 1080,
            'height' => 1080,
            'quality' => 100,
            'sharpen' => 0,
        ]
    ];

    /**
     * @param string $collectionName
     * @param string $url
     * @return mixed
     */
    public function setUrl(string $collectionName, string $url): void;

    public function setBase64(string $collectionName, string $dataBase64): void;

    public function setId(string $collectionName, int $id): void;
}
