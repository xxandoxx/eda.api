<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class UserRule implements Rule
{
    /**
     * @var int
     */
    private $userId;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userId = (int) \request()->route('user')->id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {   
        $user = User::where($attribute, $value)->where('id', '!=', $this->userId)->count();
        return $user === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Такой :attribute уже зарегистрирован';
    }
}
