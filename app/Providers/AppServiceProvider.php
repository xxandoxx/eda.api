<?php

namespace App\Providers;

use App\Models\User;
use App\Services\Cart\CartAddService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('access.city', function ($app) {
            return $this->hasAccessCity();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * @return bool
     */
    private function hasAccessCity(): bool
    {
        /** @var User $user */
        $user = request()->user();
        if (!$user) return false;
        if ($user->isSuperAdmin) return true;
        $cityId = \request('city_id')->route('city_id');
        if (!$cityId) return false;
        if ($user->hasAccessToCity($cityId)) return true;
        return false;
    }
}
