<?php

namespace App\Validation;

interface ValidationInterface
{
    /**
     * @return array
     */
    public static function indexRule(): array;

    public static function storeRule(): array;

    public static function showRule(): array;

    public static function updateRule(): array;

    public static function destroyRule(): array;
}
