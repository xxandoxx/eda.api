<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;


trait ApiResponse
{
    /**
     * Return a success JSON response.
     *
     * @param array|string $data
     * @param string|null $message
     * @param int|null $code
     * @return JsonResponse
     */
    protected function success($data, ?string $message = null, int $code = 200): JsonResponse
    {
        return response()->json([
            'message' => $message,
            'data' => $data
        ], $code,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE);

    }

    /**
     * Return an error JSON response.
     *
     * @param int $code
     * @param string|null $message
     * @param array|string|null $data
     * @return JsonResponse
     */
    protected function error(int $code, ?string $message = null, $data = null): JsonResponse
    {
        return response()->json([
            'message' => $message,
            'errors' => $data
        ],
            $code,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );

    }

}
