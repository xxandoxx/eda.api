<?php

namespace App\Services\Cart;

use App\Exception\Cart\ExceptionCart;
use App\Models\Product\Product;
use Illuminate\Http\Request;

class CartAddService
{

    public function getLineItem(Request $request)
    {
        $itemId = $request->get('item_id');
        $quantity = $request->get('quantity');
        $item = $this->getItemById($itemId);
    }

    /**
     * @throws ExceptionCart
     */
    private function getItemById(int $itemId)
    {
        $item = Product::find($itemId);
        if (!$item) {
            throw new ExceptionCart('can`t find item', 404);
        }
    }
}
