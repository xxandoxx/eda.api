<?php

namespace App\Http\Controllers\Management\Shop;

use App\Http\Controllers\ApiManagerController;
use App\Models\Shop\Shop;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ShopController extends ApiManagerController
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     tags={"[Management] Shop"},
     *     operationId="management.shop.index",
     *     path="/api/management/{cityName}/shop",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     method="application/json",
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): JsonResponse
    {
        $list = $this->city->shops()->paginate(15);

        return $this->success($list, "Success");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     tags={"[Management] Shop"},
     *     operationId="management.shop.store",
     *     path="/api/management/{cityName}/shop",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     method="application/json",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.shop.storeRole"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->validate(ShopValidation::storeRule());
        $data['city_id'] = $this->city->id;
        $data['creator_id'] = $request->user()->id;
        if (App('access.city')) {
            $data['confirmed_at'] = date('Y-m-d H:i:s');
        }
        $model = new Shop();
        $model = $model->upsert($data);

        return $this->success($model, "Success");
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     tags={"[Management] Shop"},
     *     operationId="management.shop.show",
     *     path="/api/management/{cityName}/shop/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     * )
     *
     *
     * @param string $city_name
     * @param int $id
     * @return Response
     */
    public function show($city_name, $id): JsonResponse
    {
        $model = Shop::with('products')->findOrFail($id);
        return $this->success($model, "Success");

    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Put(
     *     tags={"[Management] Shop"},
     *     operationId="management.shop.update",
     *     path="/api/management/{cityName}/shop/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.shop.updateRule"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @param string $city_name
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $city_name, $id): JsonResponse
    {        

        $data = $request->validate(ShopValidation::updateRule());
        $model = Shop::findOrFail($id);
        $model->upsert($data);
        return $this->success($model, "Success");
    }
    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete (
     *     tags={"[Management] Shop"},
     *     operationId="management.shop.delete",
     *     path="/api/management/{cityName}/shop/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=204,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param string $city_name
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($city_name, $id): JsonResponse
    {
        $model = Shop::find($id);
        if (!$model) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Магазина с таким ID не существует');
        }

        $model->delete();
        return $this->success(null, 'Success');
    }
}
