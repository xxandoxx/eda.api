<?php


namespace App\Http\Controllers\Management\Shop;


use App\Rules\CityManager;
use App\Validation\ValidationInterface;

class ShopValidation implements ValidationInterface
{

    public static function indexRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.shop.storeRole",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name"},
     *          @OA\Property(
     *              property="name",
     *              type="string",
     *              description="required, exemple: King"
     *          ),
     *         @OA\Property(
     *              property="description",
     *              type="string",
     *              description="max:2000"
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="delivery_time",
     *              type="string",
     *              description="max: 255"
     *          ),
     *         @OA\Property(
     *              property="delivery_cost",
     *              type="string",
     *              description="max: 255"
     *          ),
     *          @OA\Property(
     *              property="order_min",
     *              type="float",
     *              description="double(8,2)"
     *          ),
     *          @OA\Property(
     *              property="can_card",
     *              type="boolean",
     *              description=""
     *          ),
     *          @OA\Property(
     *              property="blocked",
     *              type="boolean",
     *              description="only superAdmin or manager of city"
     *          ),
     *          @OA\Property(
     *              property="logo",
     *              type="integer",
     *              description="image ID"
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function storeRule(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'string|nullable|max:2000',
            'active' => 'boolean',
            'delivery_time' => 'string|nullable',
            'delivery_cost' => 'string|nullable',
            'order_min' => 'numeric|nullable',
            'can_card' => 'boolean',
            'blocked' => ['boolean', new CityManager()],
            'logo' => 'nullable|integer',
        ];
    }


    public static function showRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.shop.updateRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name"},
     *          @OA\Property(
     *              property="name",
     *              type="string",
     *              description="required, exemple: King"
     *          ),
     *         @OA\Property(
     *              property="description",
     *              type="string",
     *              description="max:2000"
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="delivery_time",
     *              type="string",
     *              description="max: 255"
     *          ),
     *         @OA\Property(
     *              property="delivery_cost",
     *              type="string",
     *              description="max: 255"
     *          ),
     *          @OA\Property(
     *              property="order_min",
     *              type="float",
     *              description="double(8,2)"
     *          ),
     *          @OA\Property(
     *              property="can_card",
     *              type="boolean",
     *              description=""
     *          ),
     *          @OA\Property(
     *              property="blocked",
     *              type="boolean",
     *              description="only superAdmin or manager of city"
     *          ),
     *          @OA\Property(
     *              property="logo",
     *              type="integer",
     *              description="image ID"
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function updateRule(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'string|nullable|max:2000',
            'active' => 'boolean',
            'delivery_time' => 'string|nullable',
            'delivery_cost' => 'string|nullable',
            'order_min' => 'numeric|nullable',
            'can_card' => 'boolean',
            'blocked' => ['boolean', new CityManager()],
            'logo' => 'nullable|integer',
        ];
    }


    public static function destroyRule(): array
    {
        // TODO: Implement destroyRule() method.
    }
}
