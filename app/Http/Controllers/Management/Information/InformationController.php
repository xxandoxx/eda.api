<?php

namespace App\Http\Controllers\Management\Information;

use App\Http\Controllers\ApiManagerController;
use App\Models\Information\Information;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Validation\ValidationInterface;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class InformationController extends ApiManagerController
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     tags={"[SuperAdmin] Information"},
     *     operationId="management.information.index",
     *     path="/api/management/information",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): JsonResponse
    {
        $list = Information::all();

        return $this->success($list, "Success");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     tags={"[SuperAdmin] Information"},
     *     operationId="management.information.store",
     *     path="/api/management/information",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.information.storeRule"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->validate(InformationValidation::storeRule());
        $model = new Information();
        $model->upsert($data);
        return $this->success($model, "Success");
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     tags={"[SuperAdmin] Information"},
     *     operationId="management.information.show",
     *     path="/api/management/information/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="Information id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     * )
     *
     *
     * @param int $id
     * @return Response
     */
    public function show($id): JsonResponse
    {
        try { 
            $model = Information::findOrFail($id);
            return $this->success($model, "Success");         
        } catch (\Exception $e) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Статьи с таким ID не существует');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Put(
     *     tags={"[SuperAdmin] Information"},
     *     operationId="management.information.update",
     *     path="/api/management/information/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="Information id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.information.updateRule"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */

    public function update(Request $request, $id): JsonResponse
    {        

        $data = $request->validate(InformationValidation::updateRule());
        try {
            $model = Information::findOrFail($id);
            $model->upsert($data);
            return $this->success($model, "Success");        
        } catch (\Exception $e) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Статьи с таким ID не существует');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete (
     *     tags={"[SuperAdmin] Information"},
     *     operationId="management.information.delete",
     *     path="/api/management/information/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="Information id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=204,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param string $city_name
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        try {
            Information::findOrFail($id)->delete();
            return $this->success('Статья успешно удалена', 'Success');            
        } catch (\Exception $e) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Статьи с таким ID не существует');
        }
    }
}
