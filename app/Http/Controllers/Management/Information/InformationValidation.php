<?php


namespace App\Http\Controllers\Management\Information;


use App\Validation\ValidationInterface;

class InformationValidation implements ValidationInterface
{

    public static function indexRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.information.storeRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name", "description"},
     *          @OA\Property(
     *              property="title",
     *              type="string",
     *              description="required"
     *          ),
     *         @OA\Property(
     *              property="description",
     *              type="string",
     *              description=""
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="bottom",
     *              type="boolean",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="sort_order",
     *              type="integer",
     *              description="sort"
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function storeRule(): array
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'active' => 'boolean|nullable',
            'bottom' => 'boolean|nullable',
            'sort_order' => 'boolean|nullable',
        ];
    }


    public static function showRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.information.updateRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name", "description"},
     *          @OA\Property(
     *              property="title",
     *              type="string",
     *              description="required"
     *          ),
     *         @OA\Property(
     *              property="description",
     *              type="string",
     *              description=""
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="bottom",
     *              type="boolean",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="sort_order",
     *              type="integer",
     *              description="sort"
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */

    public static function updateRule(): array
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'active' => 'boolean|nullable',
            'bottom' => 'boolean|nullable',
            'sort_order' => 'boolean|nullable',
        ];
    }


    public static function destroyRule(): array
    {
        // TODO: Implement destroyRule() method.
    }
}
