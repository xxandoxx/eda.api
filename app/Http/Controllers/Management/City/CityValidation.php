<?php


namespace App\Http\Controllers\Management\City;


use App\Validation\ValidationInterface;
use Illuminate\Validation\Rule;

class CityValidation implements ValidationInterface
{

    public static function indexRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.city.storeRole",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name_en","name","x","y","alpha-2","country_name","country_code","time_zone"},
     *           @OA\Property(
     *              property="name_en",
     *              type="string",
     *              description="required ,primary key, can use only regex:/^[a-z\-]+$/ , exemple: shebekino"
     *          ),
     *          @OA\Property(
     *              property="name",
     *              type="string",
     *              description="required, exemple: Шебекино"
     *          ),
     *          @OA\Property(
     *              property="x",
     *              type="number",
     *              description="required,  exemple: 50.4016"
     *          ),
     *          @OA\Property(
     *              property="y",
     *              type="number",
     *              description="required, exemple: 36.9518"
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *          @OA\Property(
     *              property="country_name",
     *              type="string",
     *              description="required, exemple: Россия"
     *          ),
     *         @OA\Property(
     *              property="alpha_2",
     *              type="string",
     *              description="required, exemple: RU , https://www.iban.com/country-codes"
     *          ),
     *          @OA\Property(
     *              property="country_code",
     *              type="integer",
     *              description="required, https://en.wikipedia.org/wiki/List_of_country_calling_codes, exemple: 7"
     *          ),
     *          @OA\Property(
     *              property="federal_subject",
     *              type="string",
     *              description="exemple: Белгородская область"
     *          ),
     *          @OA\Property(
     *              property="time_zone",
     *              type="string",
     *              description="required, https://www.php.net/manual/ru/timezones.php exemple: Europe/Moscow"
     *          ),
     *          @OA\Property(property="telephones",type="array",
     *              @OA\Items(
     *                 type="object",
     *                 @OA\Property(property="telephone", type="string",description="telephone")
     *             )
     *          ),
     *          @OA\Property(property="flag",type="object",
     *              @OA\Property(property="base64", type="string",description="base64"),
     *              @OA\Property(property="url", type="string",description="url"),
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function storeRule(): array
    {
        return [
            'name_en' => [
                'string',
                'required',
                'regex:/^[a-z\-]+$/'
            ],
            'name' => 'required|string',
            'active' => 'boolean',
            'x' => 'required|numeric|between:-90,90',
            'y' => 'required|numeric|between:-180,180',
            'alpha_2' => 'required|between:2,2|regex:/^[A-Z]+$/',
            'country_name' => 'required|string',
            'country_code' => 'required|integer',
            'federal_subject' => 'string',
            'time_zone' => [Rule::in(timezone_identifiers_list()), 'required'],
            //Keywords
            'telephones' => 'array',
            'keywords.*.telephone' => 'max:255',
            'flag' => 'nullable',
            'flag.base64' => 'base64image',
            'flag.url' => 'active_url',
        ];
    }


    public static function showRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.city.updateRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *           @OA\Property(
     *              property="name_en",
     *              type="string",
     *              description="primary key, can use only regex:/^[a-z\-]+$/ , exemple: shebekino"
     *          ),
     *          @OA\Property(
     *              property="name",
     *              type="string",
     *              description="exemple: Шебекино"
     *          ),
     *          @OA\Property(
     *              property="x",
     *              type="number",
     *              description=" exemple: 50.4016"
     *          ),
     *          @OA\Property(
     *              property="y",
     *              type="number",
     *              description=" exemple: 36.9518"
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *          @OA\Property(
     *              property="country_name",
     *              type="string",
     *              description=" exemple: Россия"
     *          ),
     *         @OA\Property(
     *              property="alpha_2",
     *              type="string",
     *              description=" exemple: RU , https://www.iban.com/country-codes"
     *          ),
     *          @OA\Property(
     *              property="country_code",
     *              type="integer",
     *              description=" https://en.wikipedia.org/wiki/List_of_country_calling_codes, exemple: 7"
     *          ),
     *          @OA\Property(
     *              property="federal_subject",
     *              type="string",
     *              description="exemple: Белгородская область"
     *          ),
     *          @OA\Property(
     *              property="time_zone",
     *              type="string",
     *              description=" https://www.php.net/manual/ru/timezones.php exemple: Europe/Moscow"
     *          ),
     *          @OA\Property(property="telephones",type="array",
     *              @OA\Items(
     *                 type="object",
     *                 @OA\Property(property="telephone", type="string",description="telephone")
     *             )
     *          ),
     *          @OA\Property(property="flag",type="object",
     *              @OA\Property(property="base64", type="string",description="base64"),
     *              @OA\Property(property="url", type="string",description="url"),
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function updateRule(): array
    {
        return [
            'name_en' => [
                'string',
                'regex:/^[a-z\-]+$/'
            ],
            'name' => 'string',
            'active' => 'boolean',
            'x' => 'numeric|between:-90,90',
            'y' => 'numeric|between:-180,180',
            'alpha_2' => 'between:2,2|regex:/^[A-Z]+$/',
            'country_name' => 'string',
            'country_code' => 'integer',
            'federal_subject' => 'string',
            'time_zone' => [Rule::in(timezone_identifiers_list())],
            //Keywords
            'telephones' => 'array',
            'keywords.*.telephone' => 'max:255',
            'flag' => 'nullable',
            'flag.base64' => 'base64image',
            'flag.url' => 'active_url',

        ];
    }


    public static function destroyRule(): array
    {
        // TODO: Implement destroyRule() method.
    }
}
