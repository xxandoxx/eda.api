<?php

namespace App\Http\Controllers\Management\City;

use App\Http\Controllers\ApiManagerController;
use App\Models\City\City;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class CityController extends ApiManagerController
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     tags={"[SuperAdmin] City"},
     *     operationId="management.city.index",
     *     path="/api/management/city",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $list = City::paginate(15);

        return $this->success($list, 'Success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     tags={"[SuperAdmin] City"},
     *     operationId="management.city.store",
     *     path="/api/management/city",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.city.storeRole"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->validate(CityValidation::storeRule());
        $model = new City();
        $model->upsert($data);
        return $this->success($model, "Success");
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     tags={"[SuperAdmin] City"},
     *     operationId="management.city.show",
     *     path="/api/management/city/{cityName}",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     * )
     *
     *
     * @param City $city
     * @return JsonResponse
     */
    public function show(City $city): JsonResponse
    {
        $model = City::with('shops')->findOrFail($city->id);
        // $model = $city->with('shops')->get();
        return $this->success($model, 'Success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Put(
     *     tags={"[SuperAdmin] City"},
     *     operationId="management.city.update",
     *     path="/api/management/city/{cityName}",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.city.updateRule"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param Request $request
     * @param City $city
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, City $city): JsonResponse
    {
        $data = $request->validate(CityValidation::updateRule());
        $city->upsert($data);
        return $this->success($city, "Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete (
     *     tags={"[SuperAdmin] City"},
     *     operationId="management.city.delete",
     *     path="/api/management/city/{cityName}",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=204,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param City $city
     * @return JsonResponse
     */
    public function destroy(City $city): JsonResponse
    {
        $city->delete();
        return $this->success(null, 'Success');
    }
}
