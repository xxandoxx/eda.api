<?php

namespace App\Http\Controllers\Management\Product;

use App\Http\Controllers\ApiManagerController;
use App\Models\Product\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ProductController extends ApiManagerController
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     tags={"[Management] Product"},
     *     operationId="management.product.index",
     *     path="/api/management/{cityName}/{shop_id}/product",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *     ),
     *     method="application/json",
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $list = $this->shop->products()->paginate(30);

        return $this->success($list, "Success");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     tags={"[Management] Product"},
     *     operationId="management.product.store",
     *     path="/api/management/{cityName}/{shop_id}/product",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *     ),
     *     method="application/json",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.product.storeRole"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->validate(ProductValidation::storeRule());
        $data['shop_id'] = $this->shop->id;
        $data['creator_id'] = $request->user()->id;

        $model = new Product();
        $model = $model->upsert($data);

        return $this->success($model, "Success");
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     tags={"[Management] Product"},
     *     operationId="management.product.show",
     *     path="/api/management/{cityName}/{shop_id}/product/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop id",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Product id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     * )
     *
     *
     * @param string $city_name
     * @param int $shop_id
     * @param int $id
     * @return JsonResponse
     */
    public function show($city_name, $shop_id, $id): JsonResponse
    {
        try { 
            $model = Product::findOrFail($id);
            return $this->success($model, "Success");         
        } catch (\Exception $e) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Товара с таким ID не существует');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Put(
     *     tags={"[Management] Product"},
     *     operationId="management.product.update",
     *     path="/api/management/{cityName}/{shop_id}/product/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop id",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Product id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.product.updateRule"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @param string $city_name
     * @param int $shop_id
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $city_name, $shop_id, $id): JsonResponse
    {             
        $data = $request->validate(ProductValidation::updateRule());
        try {
            $model = Product::findOrFail($id);
            $model->upsert($data);
            return $this->success($model, "Success");        
        } catch (\Exception $e) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Товара с таким ID не существует');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete (
     *     tags={"[Management] Product"},
     *     operationId="management.product.delete",
     *     path="/api/management/{cityName}/{shop_id}/product/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop id",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Product id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=204,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param string $city_name
     * @param int $shop_id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($city_name, $shop_id, $id): JsonResponse
    {
        try {
            Product::findOrFail($id)->delete();
            return $this->success('Товар успешно удалён', 'Success');            
        } catch (\Exception $e) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Товара с таким ID не существует');
        }
    }
}
