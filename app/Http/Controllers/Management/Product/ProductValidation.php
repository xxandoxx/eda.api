<?php


namespace App\Http\Controllers\Management\Product;


use App\Rules\CityManager;
use App\Validation\ValidationInterface;

class ProductValidation implements ValidationInterface
{

    public static function indexRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.product.storeRole",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"category_id", "name","cost"},
     *          @OA\Property(
     *              property="category_id",
     *              type="integer",
     *              description="required"
     *          ),
     *          @OA\Property(
     *              property="name",
     *              type="string",
     *              description="required, example: King"
     *          ),
     *          @OA\Property(
     *              property="cost",
     *              type="integer",
     *              description="required"
     *          ),
     *          @OA\Property(
     *              property="cost_label",
     *              type="string",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="description",
     *              type="string",
     *              description="max:2000"
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="vendor_code",
     *              type="string",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="weight",
     *              type="integer",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="weight_label",
     *              type="string",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="remainder",
     *              type="integer",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="another_id",
     *              type="string",
     *              description=""
     *          ),
     *          @OA\Property(property="images",type="array",
     *              @OA\Items(
     *                  @OA\Property(property="id", type="integer",description="id"),     * 
     *              )
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function storeRule(): array
    {
        return [
            'category_id' => 'required|integer',
            'name' => 'required|string',
            'cost' => 'required|integer',
            'cost_label' => 'string|nullable',
            'description' => 'string|nullable|max:2000',
            'composition_of_goods' => 'string|nullable|max:2000',
            'active' => 'boolean',
            'vendor_code' => 'string|nullable',
            'weight' => 'integer|nullable',
            'weight_label' => 'string|nullable',
            'remainder' => 'integer|nullable',
            'another_id' => 'string|nullable',
            'images' => 'array|nullable',
            'images.*' => 'integer',
        ];
    }


    public static function showRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.product.updateRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"category_id", "name","cost"},
     *          @OA\Property(
     *              property="category_id",
     *              type="integer",
     *              description="required"
     *          ),
     *          @OA\Property(
     *              property="name",
     *              type="string",
     *              description="required, example: King"
     *          ),
     *          @OA\Property(
     *              property="cost",
     *              type="integer",
     *              description="required"
     *          ),
     *          @OA\Property(
     *              property="cost_label",
     *              type="string",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="description",
     *              type="string",
     *              description="max:2000"
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="vendor_code",
     *              type="string",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="weight",
     *              type="integer",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="weight_label",
     *              type="string",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="remainder",
     *              type="integer",
     *              description=""
     *          ),
     *         @OA\Property(
     *              property="another_id",
     *              type="string",
     *              description=""
     *          ),
     *          @OA\Property(property="images",type="array",
     *              @OA\Items(
     *                  @OA\Property(property="id", type="integer",description="id"),     * 
     *              )
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function updateRule(): array
    {
        return [
            'category_id' => 'required|integer',
            'name' => 'required|string',
            'cost' => 'required|integer',
            'cost_label' => 'string|nullable',
            'description' => 'string|nullable|max:2000',
            'composition_of_goods' => 'string|nullable|max:2000',
            'active' => 'boolean',
            'vendor_code' => 'string|nullable',
            'weight' => 'integer|nullable',
            'weight_label' => 'string|nullable',
            'remainder' => 'integer|nullable',
            'another_id' => 'string|nullable',
            'images' => 'array|nullable',
            'images.*' => 'integer',
        ];
    }


    public static function destroyRule(): array
    {
        // TODO: Implement destroyRule() method.
    }
}
