<?php

namespace App\Http\Controllers\Management\Media;

use App\Http\Controllers\ApiManagerController;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class MediaController extends ApiManagerController
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     tags={"[Management] Media"},
     *     operationId="management.media.index",
     *     path="/api/management/media",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        return $request->user()->media()->where('collection_name', 'temp')->paginate(50);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     *     tags={"[Management] Media"},
     *     operationId="management.media.store",
     *     path="/api/management/media",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="multipart/form-data",
     * @OA\RequestBody(
     *          @OA\MediaType(mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(property="file",  description="Photo Format:jpeg|png (it`s array but you can send one photo )",  type="string", format="binary"),
     *             )
     *         )
     *      ),
     * @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if($request->hasFile('file') && $request->file('file')->isValid())
        {
           $media = $request->user()->addMediaFromRequest('file')->toMediaCollection('temp');
           return $this->success($this->imageToArray($media), "Success");
        } else {
            return $this->error(422, 'Файл отсуствует или несоответствует формату данных');            
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete (
     *     tags={"[Management] Media"},
     *     operationId="management.media.delete",
     *     path="/api/management/media/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="Media id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=204,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param string $city_name
     * @param int $shop_id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(Request $request, $id)
    {   
        try {
            $request->user()->media()->findOrFail($id)->delete();
            return $this->success('Медиа-файл успешно удалён', 'Success');            
        } catch (\Exception $e) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Изображения с таким ID не существует');
        }
    }
}
