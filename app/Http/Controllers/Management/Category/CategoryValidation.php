<?php


namespace App\Http\Controllers\Management\Category;

use App\Validation\ValidationInterface;

class CategoryValidation implements ValidationInterface
{

    public static function indexRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.category.storeRole",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name"},
     *          @OA\Property(
     *              property="name",
     *              type="string",
     *              description="required"
     *          ),
     *          @OA\Property(
     *              property="order",
     *              type="integer"
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function storeRule(): array
    {
        return [
            'name' => 'required|string',
            'order' => 'numeric|nullable',
            'active' => 'boolean',
        ];
    }


    public static function showRule(): array
    {
        return [];
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.category.updateRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name"},
     *          @OA\Property(
     *              property="name",
     *              type="string",
     *              description="required"
     *          ),
     *          @OA\Property(
     *              property="order",
     *              type="integer"
     *          ),
     *          @OA\Property(
     *              property="active",
     *              type="boolean",
     *              description=""
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */

    public static function updateRule(): array
    {
        return [
            'name' => 'required|string',
            'order' => 'numeric|nullable',
            'active' => 'boolean',
        ];
    }


    public static function destroyRule(): array
    {
        // TODO: Implement destroyRule() method.
    }
}
