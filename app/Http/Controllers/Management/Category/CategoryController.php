<?php

namespace App\Http\Controllers\Management\Category;

use App\Http\Controllers\ApiManagerController;
use App\Models\Category\Category;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class CategoryController extends ApiManagerController
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     tags={"[Management] Category"},
     *     operationId="management.category.index",
     *     path="/api/management/{cityName}/{shop_id}/category",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *     ),
     *     method="application/json",
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): JsonResponse
    {
        $list = $this->shop->categories()->get();

        return $this->success($list, "Success");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     tags={"[Management] Category"},
     *     operationId="management.category.store",
     *     path="/api/management/{cityName}/{shop_id}/category",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *     ),
     *     method="application/json",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.category.storeRole"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     * @throws \Exception
     */

    public function store(Request $request): JsonResponse
    {
        $data = $request->validate(CategoryValidation::storeRule());
        $data['shop_id'] = $this->shop->id;
        
        $model = new Category();
        $model = $model->upsert($data);

        return $this->success($model, "Success");
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     tags={"[Management] Category"},
     *     operationId="management.category.show",
     *     path="/api/management/{cityName}/{shop_id}/category/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop id",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Category id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     * )
     *
     *
     * @param string $city_name
     * @param int $shop_id
     * @param int $id
     * @return JsonResponse
     */
    public function show($city_name, $shop_id, $id): JsonResponse
    {
        $model = Category::findOrFail($id);

        return $this->success($model, "Success");

    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Put(
     *     tags={"[Management] Category"},
     *     operationId="management.category.update",
     *     path="/api/management/{cityName}/{shop_id}/category/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop id",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Category id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.category.updateRule"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @param string $city_name
     * @param int $shop_id
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $city_name, $shop_id, $id): JsonResponse
    {        

        $data = $request->validate(CategoryValidation::updateRule());
        $model = Category::findOrFail($id);
        $model->upsert($data);
        return $this->success($model, "Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete (
     *     tags={"[Management] Category"},
     *     operationId="management.category.delete",
     *     path="/api/management/{cityName}/{shop_id}/category/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Shop id",
     *         in="path",
     *         name="shop_id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="Category id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=204,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param string $city_name
     * @param int $shop_id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($city_name, $shop_id, $id): JsonResponse
    {
        try {
            $model = Category::find($id);
            $model->delete();
            return $this->success(null, 'Success');
            
        } catch (\Exception $e) {
            return $this->error(null, 'Категории с таким ID не существует');            
        }
    }
}
