<?php

namespace App\Http\Controllers\Management\User;


use App\Http\Controllers\ApiManagerController;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class UserController extends ApiManagerController
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     tags={"[SuperAdmin] User"}, 
     *     operationId="management.user.index",
     *     path="/api/management/user",
     *     security={{"bearerAuth":{}}},
     *     description="Management",
     *     method="application/json",
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): Response
    { 
        $list = User::with('accessCities')->paginate(50);
        return response($list);
    }

    public function createUser(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'email_verified_at' => now()
            ]);
            return $this->success('Пользователь успешно зарегистрирован', "Success");
            
        } catch (Exception $e) {
            return $this->error(Response::HTTP_INTERNAL_SERVER_ERROR, 'Ошибка при регистрации пользователя');  
            
        }
          


    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     tags={"[SuperAdmin] User"},
     *     operationId="management.user.store",
     *     path="/api/management/user",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.user.storeRule"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->validate(UserValidation::storeRule());
        $data['password'] = Hash::make($data['password']);
        $data['email_verified_at'] = now();

        try {
            $user = User::create($data);

            return $this->success('Пользователь успешно зарегистрирован', "Success");
            
        } catch (Exception $e) {
            return $this->error(Response::HTTP_INTERNAL_SERVER_ERROR, 'Ошибка при регистрации пользователя');  
        }
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     tags={"[SuperAdmin] User"},
     *     operationId="management.user.show",
     *     path="/api/management/user/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="User id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     * )
     *
     *
     * @param int $id
     * @return Response
     */
    public function show($id): JsonResponse
    {
        try { 
            $model = User::with('accessCities')->findOrFail($id);
            return $this->success($model, "Success");         
        } catch (\Exception $e) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Пользователя с таким ID не существует');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Put(
     *     tags={"[SuperAdmin] User"},
     *     operationId="management.user.update",
     *     path="/api/management/user/{id}",
     *     security={{"bearerAuth":{}}},
     *     description="SuperAdmin",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="User id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="number",
     *         ),
     *     ),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *             ref="#/components/schemas/management.user.updateRule"
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, User $user): JsonResponse
    { 
        $data = $request->validate(UserValidation::updateRule());
        if(!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        try {

            $user->upsert($data);

            return $this->success($user, "Данные пользователя успешно обновлены");
            
        } catch (Exception $e) {
            return $this->error(Response::HTTP_INTERNAL_SERVER_ERROR, 'Ошибка при обновлении данных пользователя');  
        }
    }

    public function setSuperUser(): Response
    {

    }
}
