<?php

namespace App\Http\Controllers\Management\User;

use App\Validation\ValidationInterface;
use Laravel\Fortify\Rules\Password;
use App\Rules\UserRule;


class UserValidation implements ValidationInterface
{


    public static function indexRule(): array
    {
        // TODO: Implement indexRule() method.
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.user.storeRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name", "phone", "email","password"},
     *          @OA\Property(property="name", type="string" ),
     *          @OA\Property(property="surname", type="string" ),
     *          @OA\Property(property="patronymic", type="string" ),
     *          @OA\Property(property="birthday", type = "string", format = "date" ),
     *          @OA\Property(property="phone", type="string", description="regex:/(((7|8)[0-9]{10})|(375[0-9]{9}))/" ),
     *          @OA\Property(property="email", type="string" ),
     *          @OA\Property(property="isSuperAdmin", type="boolean", default="false" ),
     *          @OA\Property(property="password", type="string" ),
     *          @OA\Property(property="password_confirmation", type="string" ),
     *          @OA\Property(property="accessCities",type="array",
     *              @OA\Items(
     *                  @OA\Property(property="city_id", type="integer",description="city id"),
     *              )
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */

    public static function storeRule(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['nullable', 'string', 'max:80'],
            'patronymic' => ['nullable', 'string', 'max:80'],
            'birthday' => ['nullable', 'date'],
            'phone' => ['required', 'string', 'max:80', 'unique:users', 'regex:/^(((7|8)[0-9]{10})|(375[0-9]{9}))$/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed'],
            'accessCities' => 'array|nullable',
            'accessCities.*.city_id' => 'numeric',
            'isSuperAdmin' => 'boolean',
        ];
    }

    public static function showRule(): array
    {
        // TODO: Implement showRule() method.
    }

    /**
     *
     * @OA\Schema(
     *   schema="management.user.updateRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name", "phone", "email"},
     *          @OA\Property(property="name", type="string" ),
     *          @OA\Property(property="surname", type="string" ),
     *          @OA\Property(property="patronymic", type="string" ),
     *          @OA\Property(property="birthday", type = "string", format = "date" ),
     *          @OA\Property(property="phone", type="string", description="regex:/(((7|8)[0-9]{10})|(375[0-9]{9}))/" ),
     *          @OA\Property(property="email", type="string" ),
     *          @OA\Property(property="isSuperAdmin", type="boolean", default="false" ),
     *          @OA\Property(property="password", type="string" ),
     *          @OA\Property(property="password_confirmation", type="string" ),
     *          @OA\Property(property="accessCities",type="array",
     *              @OA\Items(
     *                  @OA\Property(property="city_id", type="integer",description="city id"),
     *              )
     *          ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function updateRule(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['nullable', 'string', 'max:80'],
            'patronymic' => ['nullable', 'string', 'max:80'],
            'birthday' => ['nullable', 'date'],
            'phone' => ['required', 'string', 'max:80', 'regex:/^(((7|8)[0-9]{10})|(375[0-9]{9}))$/', new UserRule],
            'email' => ['required', 'string', 'email', 'max:255', new UserRule],
            'password' => ['string', 'confirmed', 'nullable'],
            'accessCities' => 'array|nullable',
            'accessCities.*.city_id' => 'numeric',
            'isSuperAdmin' => 'boolean',
        ];
    }

    public static function destroyRule(): array
    {
        // TODO: Implement destroyRule() method.
    }
}
