<?php


namespace App\Http\Controllers;


use App\Models\City\City;
use App\Models\Shop\Shop;


class ApiManagerController extends ApiController
{
    protected ?City $city = null;
    protected ?Shop $shop = null;

    public function __construct(City $city, Shop $shop)
    {

        $cityName = \request()->route('city_name');
        $shopId = \request()->route('shop_id');
        
        if ($cityName) {
            $this->city = City::where('name_en', $cityName)->first();
        }

        if ($shopId) {
            $this->shop = $shop->findOrFail($shopId);
        }
        
    }


    public function imageToArray(object $model): array
    {
        $item = [];
        $item['id'] = $model->id;
        $item['small'] = $model->getUrl('small') ?? null;
        $item['medium'] = $model->getUrl('medium') ?? null;
        $item['large'] = $model->getUrl('large') ?? null;
        $item['original'] = $model->original_url;

        return $item;

    }
    /**
     * @OA\SecurityScheme(
     *      securityScheme="bearerAuth",
     *      in="header",
     *      name="ManagerToken",
     *      type="http",
     *      scheme="bearer",
     *      bearerFormat="JWT",
     * )
     */

}
