<?php

namespace App\Http\Controllers\Client\User;

use App\Http\Controllers\ApiClientController;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Jetstream\Jetstream;

class UserController extends ApiClientController
{


    /**
     * Registration user.
     *
     * @OA\Post(
     *     tags={"[Global] User"},
     *     operationId="client.user.registration",
     *     path="/api/user/registration",
     *     description="Global",
     *     method="multipart/form-data",
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(ref="#/components/schemas/clinet.user.registrationRule"),
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */

    public function registration(Request $request): JsonResponse
    {
        $data = $request->validate(UserValidation::registrationRule());
        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'] ?? null,
            'patronymic' => $data['patronymic'] ?? null,
            'birthday' => $data['birthday'] ?? null,
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        if ($user instanceof MustVerifyEmail) {
            $user->sendEmailVerificationNotification();
            return $this->success(null, 'Подтвердите ваш электронный адрес перейдя по ссылке в письме', self::HTTP_EMAIL_MUST_BE_VERIFICATION);
        }
        return $this->success($user);
    }

    /**
     * Login user.
     *
     * @OA\Post(
     *     tags={"[Global] User"},
     *     operationId="client.user.login",
     *     path="/api/user/login",
     *     description="Global",
     *     method="multipart/form-data",
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(ref="#/components/schemas/clinet.user.loginRule"),
     *          )
     *      ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */

    public function login(LoginRequest $request): JsonResponse
    {
        try {
            $request->authenticate();
            $user = $request->user();
            if ($user instanceof MustVerifyEmail) {
                if (!$user->hasVerifiedEmail()) {
                    return $this->error(self::HTTP_EMAIL_ADDRESS_IS_NOT_VERIFIED, 'Ваш E-mail адрес не подтверждён');
                }
            }
        } catch (\Throwable $exception) {
            return $this->error(Response::HTTP_UNAUTHORIZED, 'Не верно введён E-mail или пароль');
        }


        $user = auth()->user();
        $user['token'] = explode('|', auth()->user()->createToken(date('Y-m-d H:i:s'), Jetstream::validPermissions([]))->plainTextToken)[1];

//         $token = $user->createToken(
//             date('Y-m-d H:i:s'),
//             Jetstream::validPermissions([])
//         );

        return $this->success($user);
    }

    /**
     * Verify-email user.
     *
     * @OA\Get(
     *     tags={"[Global] User"},
     *     operationId="client.user.verifyEmail",
     *     path="/api/user/verifyEmail/{id}/{hash}",
     *     description="Global",
     *     method="multipart/form-data",
     *     @OA\Parameter(
     *         description="User Id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="hash",
     *         in="path",
     *         name="hash",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @param Request $request
     * @param int $id
     * @param string $hash
     * @return Response
     */

    public function verifyEmail(Request $request, int $id, string $hash): Response
    {
        $user = User::find($id);
        if ($user->hasVerifiedEmail()) {
            return $this->success(null, 'Ваш почтовый адрес уже подтверждён');
        }

        if (!hash_equals($hash,
            sha1($user->getEmailForVerification()))) {
            return $this->error(self::HTTP_EMAIL_ADDRESS_IS_NOT_VERIFIED, 'Ваш E-mail адрес не подтверждён');
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return $this->success(null, 'Ваш почтовый адрес успешно подтверждён');
    }
}
