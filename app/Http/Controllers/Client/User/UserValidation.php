<?php

namespace App\Http\Controllers\Client\User;

use Laravel\Fortify\Rules\Password;

class UserValidation
{

    /**
     *
     * @OA\Schema(
     *   schema="clinet.user.registrationRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"name", "phone", "email","password"},
     *          @OA\Property(property="name", type="string" ),
     *          @OA\Property(property="surname", type="string" ),
     *          @OA\Property(property="patronymic", type="string" ),
     *          @OA\Property(property="birthday", type = "string", format = "date" ),
     *          @OA\Property(property="phone", type="string", description="regex:/(((7|8)[0-9]{10})|(375[0-9]{9}))/" ),
     *          @OA\Property(property="email", type="string" ),
     *          @OA\Property(property="password", type="string" ),
     *          @OA\Property(property="password_confirmation", type="string" ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function registrationRule(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['nullable', 'string', 'max:80'],
            'patronymic' => ['nullable', 'string', 'max:80'],
            'birthday' => ['nullable', 'date'],
            'phone' => ['required', 'string', 'max:80', 'unique:users', 'regex:/^(((7|8)[0-9]{10})|(375[0-9]{9}))$/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed'],
        ];
    }

    /**
     *
     * @OA\Schema(
     *   schema="clinet.user.loginRule",
     *   type="object",
     *   allOf={
     *       @OA\Schema(
     *          required={"email","password"},
     *          @OA\Property(property="email", type="string", ),
     *          @OA\Property(property="password", type="string", ),
     *      ),
     *
     *   }
     * )
     *
     */
    public static function loginRule(): array
    {
        return [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ];
    }
}
