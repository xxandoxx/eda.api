<?php

namespace App\Http\Controllers\Client\Cart;

use App\Http\Controllers\ApiClientController;
use App\Services\Cart\CartAddService;
use App\Services\Cart\CartService;
use Illuminate\Http\Request;

class CartController extends ApiClientController
{

    public function add(CartAddService $service, Request $request)
    {
        $service->getLineItem($request);
    }

}
