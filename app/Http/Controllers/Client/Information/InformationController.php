<?php

namespace App\Http\Controllers\Client\Information;

use App\Http\Controllers\ApiClientController;
use App\Models\Information\Information;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class InformationController extends ApiClientController
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     tags={"[Global] Information"},
     *     operationId="client.information.index",
     *     path="/api/information",
     *     description="Management",
     *     method="application/json",
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $list = Information::where([['active', '=', 1],['bottom', '=', 1]])->orderBy('sort_order', 'ASC')->get();

        return $this->success($list, 'Success');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     tags={"[Global] Information"},
     *     operationId="client.information.show",
     *     path="/api/information/{id}",
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="ID",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     * )
     *
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        try { 
            $model = Information::findOrFail($id);
            return $this->success($model, "Success");         
        } catch (\Exception $e) {
            return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'Статьи с таким ID не существует');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
