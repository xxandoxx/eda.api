<?php

namespace App\Http\Controllers\Client\City;

use App\Http\Controllers\ApiClientController;
use App\Models\City\City;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class CityClientController extends ApiClientController
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     tags={"[Global] City"},
     *     operationId="client.city.index",
     *     path="/api/city",
     *     description="Management",
     *     method="application/json",
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\MediaType(
     *         mediaType="application/json"
     *     )
     * ),
     *)
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $list = City::select('id', 'name', 'name_en', 'country_name', 'federal_subject')->where('active', 1)->get();

        return $this->success($list, 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     tags={"[Global] City"},
     *     operationId="client.city.show",
     *     path="/api/city/{cityName}",
     *     description="Management",
     *     method="application/json",
     *     @OA\Parameter(
     *         description="cityName",
     *         in="path",
     *         name="cityName",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *         ),
     *     ),
     *     @OA\Response(
     *     response=200,
     *     description="successful operation",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     * )
     *
     *
     * @param City $city
     * @return JsonResponse
     */
    public function show(City $city): JsonResponse
    {      
        return $this->success($city->load('shops'), 'Success');
    }
}
