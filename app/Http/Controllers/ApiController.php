<?php


namespace App\Http\Controllers;


use App\Traits\ApiResponse;

class ApiController extends Controller
{
    use ApiResponse;

    /**
     * @SWG\Swagger(
     *     schemes=["https"],
     *     host=API_HOST,
     *     basePath="/",
     *     @OA\Info(
     *         version="1.0.0",
     *         title="Edaa.ru",
     *         description="API Documents",
     *         termsOfService="",
     *         @OA\Contact(
     *             email="k.andranik@mail.ru"
     *         ),
     *     ),
     * )
     */


    /**
     * For help by status
     * @\Symfony\Component\HttpFoundation\Response
     */
    public const HTTP_EMAIL_ADDRESS_IS_NOT_VERIFIED = 460;
    public const HTTP_EMAIL_MUST_BE_VERIFICATION = 231;
}
