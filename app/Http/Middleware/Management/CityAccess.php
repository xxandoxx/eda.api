<?php

namespace App\Http\Middleware\Management;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\City\City;
use Exception;

class CityAccess
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request,  Closure $next)
    {
        /** @var \App\Models\User $user */


        $user = $request->user();
        $cityName = $request->route('city_name');


        if ($cityName && $user->hasAccessToCity($cityName)) {
            return $next($request);
        }

       /* try {
            $city = City::where('name_en', $cityName)->firstOrFail();

        } catch (Exception $e) {

            return \response('Город не найден', Response::HTTP_FORBIDDEN);
        }

        if ($user->isSuperAdmin && $city && $city->id) {

            return $next($request);

        } else if($city && $city->id && $user->hasAccessToCity($city->id)) {

            return $next($request);     

        }*/
        // $cityId = (int)$request->route('city_id');
        // if ($city && $city->id && $user->hasAccessToCity($city->id)) {
        //     return $next($request);
        // }
//        $shopId = (int)$request->route('shop_id');
//        if ($shopId && $user->hasAccessToShop($shopId)) {
//            return $next($request);
//        }

        return \response('У Вас нет прав для управления этим городом', Response::HTTP_FORBIDDEN);
    }
}
